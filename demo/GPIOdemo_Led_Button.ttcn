///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
//
// Contributors:
//   Gyorgy Rethy
// version R2A
///////////////////////////////////////////////////////////////////////////////

module GPIOdemo_Led_Button {

import from GPIOPinPort all;
// Files TCCFileIO_Functions.ttcn and TCCFileIO.cc shall be added to the project
// to compile it! They can be found in the project TCC Useful Functions:
// git clone https://github.com/eclipse/titan.Libraries.TCCUsefulFunctions.git
import from TCCFileIO_Functions all;

modulepar float tsp_Tshort := 0.1, tsp_TLED := 1.5, tsp_Twait := 10.0;
modulepar integer pinLED, pinButton;
modulepar boolean debug := false;

type component GPIO extends GPIO_Base {
	var charstring exportdirLED := "/sys/class/gpio/gpio" & int2str(pinLED);
	var charstring directionfileLED := "/sys/class/gpio/gpio" & int2str(pinLED) & "/direction";
	var charstring valuefileLED := "/sys/class/gpio/gpio" & int2str(pinLED) & "/value";

	var charstring exportdirButton := "/sys/class/gpio/gpio" & int2str(pinButton);
	var charstring directionfileButton := "/sys/class/gpio/gpio" & int2str(pinButton) & "/direction";
	var charstring valuefileButton := "/sys/class/gpio/gpio" & int2str(pinButton) & "/value";

	timer T_guard := 15.0;
}

function f_checkDirectory(charstring p_exportdir) runs on GPIO {
	if (f_FIO_fileOrDirExists(p_exportdir)) {
	  log("Export directory created successfully");
	  setverdict (pass);	  
	} else {
	  setverdict (fail,"Failed to create export directory");
	  mtc.stop
	}
}

function f_setDirection(
  integer p_pin,
  GPIO_PIN_DIRECTION p_direction
) runs on GPIO {

	gpio[p_pin].send(p_direction);

}

function f_checkDirection(
  charstring p_directionfile,
  GPIO_PIN_DIRECTION p_direction
) runs on GPIO {
	if (debug) {
		var integer fd := f_FIO_open_rdonly(p_directionfile);
		var charstring directionExpected;
		var charstring direction;
		var integer ret := f_FIO_read_text_until(fd, direction, "\n");
	
		select (p_direction) {
		  case (OUT) { directionExpected := "out" }
		  case (IN)  { directionExpected := "in" }
		}
	
		  if (direction == directionExpected) {
		    setverdict (pass,"Direction set to ",p_direction," successfully");
		  } else {
		    setverdict (fail,"Direction is not ",p_direction,": %s", direction);
		    mtc.stop
		  }

		f_FIO_close(fd);
	}
}

function f_setValue(
  integer p_pin,
  GPIO_PIN_VALUE p_value
) runs on GPIO {
	gpio[p_pin].send(p_value);
}

function f_checkValue(
  charstring p_valuefile,
  GPIO_PIN_VALUE p_value
) runs on GPIO {
	if (debug) {
		var integer fd := f_FIO_open_rdonly(p_valuefile);
		var charstring value_ := "", valueExpected;
		var integer ret := f_FIO_read_text_until(fd, value_, "\n");
	
		select (p_value) {
	  		case (HIGH) { valueExpected := "1" }
	  		case (LOW)  { valueExpected := "0" }
		}

		if (value_ == valueExpected) {
	  		log("Pin value set to ", p_value, " successfully");
	  		setverdict (pass);
		} else {
	  		log("Pin value is not ", p_value,": %s", value_);
	  		setverdict (fail);
	  		mtc.stop;
		}
	
		f_FIO_close(fd);
	}
}

function f_delay(float p_delay) {
	timer T_delay := p_delay;
	T_delay.start;
	T_delay.timeout;
}

function f_LED_on_off(integer p_pin,float p_timeOn) runs on GPIO {

	f_setValue(p_pin,HIGH);
	f_checkValue(valuefileLED,HIGH);

	f_delay(p_timeOn);

	f_setValue(p_pin,LOW);
	f_checkValue(valuefileLED,LOW)
}

altstep as_removeState(integer p_pin, GPIO_PIN_VALUE p_value) runs on GPIO {
	[] gpio[p_pin].receive(p_value) {
	  log("pin ",p_pin," is pulled to ",p_value);
	  repeat;
	}
}

testcase TC_LEDon() runs on GPIO {
	
	//Map the port, this will export the gpio	
	map(self:gpio[pinLED], system:gpio[pinLED]);

	f_delay(tsp_Tshort);

	f_checkDirectory(directionfileLED)	

	f_delay(tsp_Tshort);
	
	//Set direction on the gpio to OUT
    f_setDirection(pinLED,OUT)
    f_checkDirection(directionfileLED,OUT);

	f_LED_on_off(pinLED,tsp_TLED)
		
	unmap(self:gpio[pinLED], system:gpio[pinLED]);
	
	setverdict(pass);
}

testcase TC_button() runs on GPIO {
	timer T_wait := tsp_Twait;	

	//Mapping ports, this will export the gpios	
	map(self:gpio[pinLED], system:gpio[pinLED]);
	map(self:gpio[pinButton], system:gpio[pinButton]);

	f_delay(tsp_Tshort);

	f_checkDirectory(directionfileLED);
	f_checkDirectory(directionfileButton);

	f_delay(tsp_Tshort);

	//Set direction on the gpio21 to OUT
    f_setDirection(pinLED,OUT)
    f_checkDirection(directionfileLED,OUT);

	//Switch the LED on gpio21 ON
	f_setValue(pinLED,HIGH);
	f_checkValue(valuefileLED,HIGH);

	//Set direction on the gpio[pinButton] to IN
    f_setDirection(pinButton,IN)
    f_checkDirection(directionfileButton,IN);

//	var default v_defRemoveState := activate(as_removeState(pinButton,HIGH));
	
	action("Start wait for signal to appear...");
	
	T_wait.start;

	alt {
	  []  gpio[pinButton].receive(GPIO_PIN_VALUE:LOW){
	  	log("Input changed to \"LOW\"");
	  	T_wait.stop;
		f_setValue(pinLED,LOW);
		f_checkValue(valuefileLED,LOW);
	  	setverdict (pass);
		f_delay(2.0)//waiting before killing test case to see button shaking messages!
	  }
	  [] T_wait.timeout {
	  	log("Timeout while waiting for input to change");
		f_setValue(pinLED,LOW);
		f_checkValue(valuefileLED,LOW);
		f_delay(0.5);f_LED_on_off(pinLED,0.5);
		f_delay(0.5);f_LED_on_off(pinLED,0.5);
		f_delay(0.5);f_LED_on_off(pinLED,0.5);	
	  	setverdict(inconc);
	  }
	}
	  
	//Unmap the port, this will unexport the gpio
	log("Unmapping ports");
			
	unmap(self:gpio[pinLED], system:gpio[pinLED]);
	unmap(self:gpio[pinButton], system:gpio[pinButton]);

}

control {
  execute(TC_LEDon());
  execute(TC_button());
}

}
